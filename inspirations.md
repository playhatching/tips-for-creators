Some inspirations:

* [_The Bug_ by Ellen Ullman](https://us.macmillan.com/thebug/ellenullman/9781250002495/)

* [Python fanfic](https://github.com/oboechick/pyfanfic)

* zines like [Bubblesort](https://shop.bubblesort.io/) and [Julia Evans's Wizard Zines](https://wizardzines.com/)

* [K. Lars Lohn's PyCon 2016 keynote](https://www.youtube.com/watch?v=bSfe5M_zG2s)

* ["Python Grab Bag: A Set of Short Plays"](https://www.harihareswara.net/sumana/2018/10/06/0) (some [making-of thoughts](https://www.harihareswara.net/sumana/2018/09/20/0))

* ["Code Review: Forwards and Back"](https://www.harihareswara.net/sumana/2017/11/11/0) (some [making-of thoughts](https://www.harihareswara.net/sumana/2017/10/02/0))

* the [aesthetic](https://recompilermag.com/issues/extras/toward-a-bangbangcon-aesthetic/) of [!!Con](http://bangbangcon.com/)

* [Annalee Flower Horne's `CARBORUNDORUM > /DEV/NULL`](https://firesidefiction.com/carborundorum-dev-null)

* [think tank fiction (examples)](https://www.harihareswara.net/sumana/2018/09/04/0)

* [xkcd](https://xkcd.com/) ([example](https://xkcd.com/936/))

* [Sammus (rapper)](https://en.wikipedia.org/wiki/Sammus)

* ["Code Monkey"](https://wiki.jonathancoulton.com/Code_Monkey) by Jonathan Coulton

* _Dive Into Accessibility_ personas: [intro](https://web.archive.org/web/20110927132031/http://diveintoaccessibility.org/introduction.html), [Jackie](https://web.archive.org/web/20110927131503/http://diveintoaccessibility.org/day_1_jackie.html), [Michael](https://web.archive.org/web/20110927131830/http://diveintoaccessibility.org/day_2_michael.html), [Bill](https://web.archive.org/web/20110927131834/http://diveintoaccessibility.org/day_3_bill.html), [Lillian](https://web.archive.org/web/20110927131818/http://diveintoaccessibility.org/day_4_lillian.html), [Marcus](https://web.archive.org/web/20110927131519/http://diveintoaccessibility.org/day_5_marcus.html)

* ["Mallory"](https://www.crummy.com/writing/Mallory/) & ["Constellation Games"](http://constellation.crummy.com/) by Leonard Richardson

* the first few chapters of [D R MacIver's Vinge fanfic](https://archiveofourown.org/works/9233966/chapters/20941043)

* ["The Divine Comedy of the Tech Sisterhood" by Anat Deracine](https://code.likeagirl.io/the-divine-comedy-of-the-tech-sisterhood-62bf54fd2186)

* ["Pipeline", a video art collage by Sumana Harihareswara](https://brainwane.dreamwidth.org/2015/05/23/pipeline-vid.html)

* [the Society of Actuaries scifi contest](https://www.soa.org/sections/2019-speculative-fiction-contest/)
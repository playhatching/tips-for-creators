How to write, rehearse, and perform a play
==========================================

*This **work-in-progress** guide is meant to demystify the process of writing a short play and putting it on during [The Art of Python](https://us.pycon.org/2019/hatchery/artofpython/) at PyCon 2019. It's meant for people who don't have experience playwriting, acting, directing, etc.*

**[Getting an idea](#coming-up-with-an-idea)** |
**[Finding collaborators](#finding-collaborators)** |
**[Budgeting](#budgeting)** |
**[Writing](#writing)** |
**[Rehearsing](#rehearsing)** |
**[Performing](#performing)** | **[Examples](#examples)**

Coming up with an idea
----------------------

Look at the topics covered in [our inspirations](inspirations.md). Or: we'd love:

* fictional stories/scenarios, such as composites of real situations or future projections, reflecting on and demonstrating the effects of particular language features, policies and trends
* musical and/or dance performances that explore CS, programming, or interpersonal experiences in engineering
* docudrama-style retellings of important decisions in the history of Python
* spectacularly ambitious demonstrations of innovative software and hardware
* visual art, static or animated, about the experience of programming

If none of those speak to you:

* Use [this observational comedy HOWTO](https://www.harihareswara.net/sumana/2005/12/09/0) to find and explore experiences you often complain about or tell stories about.
* When in your programming experiences do you often feel uncertain or conlicted?
* When in your programming experiences have you felt a feeling of achievement or triumph?

Maybe you'll come up with the topic first -- maybe first you'll think of a way of performing and presenting it (examples: one long dialogue; a set of monologues; dialogues interrupted by songs; wordless dance with props; live-coding during dialogue).

Feel free to email us at `sh@changeset.nyc` to bounce your idea around and ask us for help!

Finding collaborators
---------------------

You will need four basic kinds of work to make a play.

* Playwriting
* Acting
* Directing: advising actors on how to move their bodies, use props, and use their voices to convey the story effectively
* Logistical/project management work (stage management, wardrobe, props, light, and sound)

You probably need at least two people total; even if one person is writing the play and doing all the acting, and even if you have very few props or sound and light cues, it's super useful to have someone else give feedback as a director and keep track of times, dates, and things.

Find at least one other person to work with. The Art of Python organizers can help match-make if you tell us what you want to do and what you need help with.

*Do the steps above before submitting your final proposal to The Art Of Python on papercall.io.*

Budgeting
---------

You may need or choose to pay for:

* skilled staff (especially directors): you'll want at least an hour of their time for every 2 minutes of the finished piece, and budget at least $50/hour in the US
* props
* rehearsal space

Try to make a budget as soon as you can. If you need financial help, The Art of Python organizers can possibly help you with a few hundred dollars, or help find free props and rehearsal space and some donated labor; let us know what you need.

Some sample budgets are in [the example work directory](https://gitlab.com/playhatching/tips-for-creators/tree/master/example-works).

Writing
-------

For "The Art of Python" you need to get the start of a draft on paper by mid-March.

You'll probably want to get at least a few minutes of script on paper before you start rehearsing. You may find an agile approach works best -- as you rehearse, you'll explore, revise, and add. Or you may feel more comfortable getting a whole draft script on paper before rehearsing.

Check out [the example work directory](https://gitlab.com/playhatching/tips-for-creators/tree/master/example-works) for sample scripts!

The Art of Python organizers can personally coach you a bit on this part -- just ask.

Rehearsing
----------

Actors and directors need to rehearse your play several times before the May 3rd performance. The more people who can attend rehearsals, the better, including writers and stage managers.

We advise that you:

1. *Mid-March*: read aloud the script after the writer has a draft down, to test how it sounds and iterate
2. *Mid-March*: have actors read the script aloud (a "table reading") a few times, and iterate the script in response
3. *Late March*: have the director, stage manager, and actors start figuring out where the actors should stand and move during each scene ("blocking" the play) and finish figuring out props, sound and light cues, as the actors rehearse the play, and finish iterating; this will take a few rehearsals 
4. *Early April*: have the actors memorize the script and get "off-book" and rehearse a few times without looking at the script, while they practice blocking and working with props, and the stage manager practices sound/light cues
5. *Mid-April*: rehearse the play while videocalling with "Art of Python" organizers, to confirm timing and readiness, and help organizers finalize festival sequence/schedule
6. *Late April*: perform the play in front of a test audience of about 10-20 people

William Ball's book "A Sense of Direction" has the useful reminder that, in the first off-book rehearsal and the first full lights-and-sound-and-dress rehearsal, it is natural for everything to snag and drag. That's ok. Take notes, keep going, and it'll be better the next time.

Performing
----------

If you haven't attended a play in years, find a production to attend -- even if it's only a local school play -- to remind you of what to expect.

If you haven't acted in live theater at all, or in several years, it's particularly important to hire a director. A college student who's studying theater might be an inexpensive choice, and will still massively increase the quality of the acting and blocking.

Reflect on performances you've enjoyed (plays, TV, movies, etc.). What did you like about them? See what you can bring into your own performance.

Plan to have an informal chat with your test audience after the test performance to find out what made sense to them, what felt off, and so on.

Examples
--------

See [the example work directory](https://gitlab.com/playhatching/tips-for-creators/tree/master/example-works) for sample scripts, budgets, time estimates, and so on!